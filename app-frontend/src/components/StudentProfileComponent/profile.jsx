import React from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle
} from "reactstrap";
import "./profile.css";

const Profile = ({ t, i18n }) => {
  return (
    <div className={"profile-container"}>
      <div className={"news-header"}>{t("title-student")}</div>
      <div className={"contact-container"}>
        <Card className={"profile-card"}>
          <CardImg
            top
            width="100%"
            src="/Poza.jpg"
            alt="Card image cap"
            className={"image"}
          />
          <CardBody>
            <CardTitle>Calin Florescu</CardTitle>
            <CardSubtitle>{t("interest")}</CardSubtitle>
            <CardText>
              Web Development, algoritmi, eficientizare, management
            </CardText>
          </CardBody>
        </Card>
      </div>
    </div>
  );
};

export default Profile;
