from flask import Flask, request
from flask_mail import Mail, Message
from flask_cors import CORS, cross_origin
from pathlib import Path
import xmltodict
import json

app = Flask(__name__)
cors = CORS(app)

app.config['CORS_HEADERS'] = 'Content-Type'

app.config.update(dict(
    DEBUG = True,
    MAIL_SERVER = 'smtp.gmail.com',
    MAIL_PORT = 465,
    MAIL_USE_SSL = True,
    MAIL_USERNAME = 'projectdaw2@gmail.com',
    MAIL_PASSWORD = 'dawproject',
))

mail = Mail(app)

@app.route('/message', methods = ['POST'])
@cross_origin()
def sendMessage():
	if request.method == 'POST':
		content = request.json
		print(content['email'])
		# la recipients se poate adauga orice adresa de email pentru a testa functionalitatea 
		msg = Message('Message from Daw app',
			sender="from@example.com",
			recipients=[content['email']])
		msg.body = content['message']
		mail.send(msg)
		return 'Message sent!'
	else: 
		return 'Method not allowed'

@app.route('/news', methods = ['GET'])
@cross_origin()
def getNews():
	if request.method == 'GET':
		xml = Path('./Xml-files/news.xml').read_text()
		doc = xmltodict.parse(xml)

		return json.dumps(doc)
